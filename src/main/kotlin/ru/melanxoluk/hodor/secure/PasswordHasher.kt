package ru.melanxoluk.hodor.secure

import org.koin.core.component.KoinComponent
import java.security.MessageDigest


class PasswordHasher(private val salt: ByteArray): KoinComponent {
    fun hash(password: String): String {
        val messageDigest = MessageDigest.getInstance("SHA-512") // todo config value
        messageDigest.update(salt)
        val hashBytes = messageDigest.digest(password.toByteArray())
        return printableHexString(hashBytes)
    }

    private fun printableHexString(digestedHash: ByteArray): String {
        return digestedHash.map { Integer.toHexString(0xFF and it.toInt()) }
            .map { if (it.length < 2) "0$it" else it }
            .fold("", { acc, d -> acc + d })
    }
}
