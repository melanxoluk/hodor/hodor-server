package ru.melanxoluk.hodor

import com.sksamuel.hoplite.ConfigFilePropertySource
import com.sksamuel.hoplite.ConfigLoader
import com.sksamuel.hoplite.ConfigSource
import java.io.File


data class ApplicationConfig(
    val key: String,
    val superUser: SuperUserConfig,
    val database: DatabaseConfig) {

    companion object {
        operator fun invoke(): ApplicationConfig {
            val source =
                ConfigFilePropertySource(
                    ConfigSource.FileSource
                        (File("application.yaml"))
                )

            return ConfigLoader.Builder()
                .addPropertySource(source)
                .build()
                .loadConfigOrThrow()
        }
    }
}

data class SuperUserConfig(
    var login: String,
    var password: String
)

data class DatabaseConfig(
    val jdbc: String,
    val user: String,
    val password: String
)